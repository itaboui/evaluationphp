<?php  

$uri = $_SERVER['REQUEST_URI'];


if($result = match($uri, "/user/create")){
    require("../Controller/user/createUser.php");
    die;
}



if($result = match($uri, "/user/update/:id")){
    require("../Controller/user/updateUser.php");
    die;
}



if($result = match($uri, "/users")){
    require("../Controller/user/displayUsers.php");
    die;
}


if($result = match($uri, "/user/:id")){
    require("../Controller/user/displayUser.php");
    die;
}

if($result = match($uri, "/user/delete/:id")){
    require("../Controller/user/deleteUser.php");
    die;
}



if($result = match($uri, "/rental/create")){
    require("../Controller/rental/createRental.php");
    die;
}



if($result = match($uri, "/rental/:id")){
    require("../Controller/rental/displayRental.php");
    die;
}


if($result = match($uri, "/rentals")){
    require("../Controller/rental/displayRentals.php");
    die;
}


if($result = match($uri, "/rental/update/:id")){
    require("../Controller/rental/updateRental.php");
    die;
}


if($result = match($uri, "/rental/delete/:id")){
    require("../Controller/rental/deleteRental.php");
    die;
}

























#fonction match retourne vrai si la route est bonne. si la route est bonne on require le fichier qu'il nous faut .

function match($url, $route){
   $path = preg_replace('#:([\w]+)#', '([^/]+)', $route);

   $regex = "#^$path$#i";
   if(!preg_match($regex, $url, $matches)){
       return false;
   }
   return true;
}
