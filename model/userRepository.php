<?php  




function getUsers()
{
	$bdd = dbConnect() ;


	$response = $bdd->prepare ('SELECT u.id, u.email FROM user u WHERE 1') ;

	$response->execute(array());

	return $response;
}

function getUser($userId)
{
	$bdd = dbConnect();

	$response = $bdd->prepare ('SELECT * FROM user 
		WHERE id = :userId') ;

	$response->execute(array('userId' => $userId));

	return $response;
}



function createUser ($email ,$pseudo ,$password)
{

	$bdd = dbConnect();

	$encryptedPassword = password_hash($password, PASSWORD_DEFAULT);
	$response = $bdd->prepare ('INSERT INTO user(`email`,`pseudo`, `password`)
		VALUES (:email, :pseudo, :password)');

	$response->execute(array('email' => $email,'pseudo' => $pseudo, 'password' => $encryptedPassword));



}


function updateUser ($userId, $email, $pseudo, $password)
{

    $bdd = dbConnect();
    $encryptedPassword = password_hash($password, PASSWORD_DEFAULT);

    $response = $bdd->prepare ('UPDATE `user` SET 
                                `email`= :email ,
                                `pseudo`= :pseudo ,
                                `password`= :password
                                WHERE id = :userId') ;

    $response->execute(array(   'userId' => $userId,
                                'email' => $email,
                                'pseudo' => $pseudo,
                                'password' => $password,
                                
    ));
    return $response;

}



function deleteArticle($userId){
    $bdd = dbConnect();

    $response = $bdd->prepare ('DELETE FROM `user` WHERE id= :userId') ;

    $response = $response->execute(array(   'userId' => $userId));

}







function dbConnect(){
	try
	{
		return new PDO('mysql:host=localhost;dbname=rental;charset=utf8', 'root', '');
	}
	catch(Exception $e)
	{
		die('Erreur : '.$e->getMessage());
	}
}

