<?php  

function getRental($rentalId)
{
	$bdd = dbConnect();

	$response = $bdd->prepare ('SELECT * FROM rentale 
		WHERE id = :rentalId') ;

	$response->execute(array('rentalId' => $rentalId));

	return $response;
}



function getRentals()
{
	$bdd = dbConnect() ;


	$response = $bdd->prepare ('SELECT * FROM rentale  WHERE 1') ;

	$response->execute(array());

	return $response;
}


function createRental ($userId ,$title ,$body ,$rentalstart ,$rentalfinish ,$author)
{

	$bdd = dbConnect();

	$response = $bdd->prepare ('INSERT INTO rentale(`userid`, `title`, `body`, `rentalstart`, `rentalfinish`, `author`)
		VALUES (:userid, :title, :body, :rentalstart, :rentalfinish, :author)');

	$response->execute(array('userid' => $userId,'title' => $title,'body' => $body,'rentalstart' => $rentalstart,'rentalfinish' => $rentalfinish,'author' => $author));



}



function updateRental ($rentalId ,$title ,$body ,$rentalstart ,$rentalfinish ,$author)
{

    $bdd = dbConnect();

    $response = $bdd->prepare ('UPDATE `rentale` SET 
                                `title`= :title ,
                                `body`= :body ,
                                `rentalstart`= :rentalstart ,
                                `rentalfinish`= :rentalfinish ,
                                `author`= :author
                                WHERE id = :rentalId') ;

    $response->execute(array(   'rentalId' => $rentalId,
                                'title' => $title,
                                'body' => $body,
                                'rentalstart' => $rentalstart,
                                'rentalfinish' => $rentalfinish,
                                'author' => $author,
                                
    ));

}



function deleteRental($rentalId){
    $bdd = dbConnect();

    $response = $bdd->prepare ('DELETE FROM `rentale` WHERE id= :rentalId') ;

    $response = $response->execute(array(   'rentalId' => $rentalId));

}































function dbConnect(){
	try
	{
		return new PDO('mysql:host=localhost;dbname=rental;charset=utf8', 'root', '');
	}
	catch(Exception $e)
	{
		die('Erreur : '.$e->getMessage());
	}
}

